import re


class IDException(Exception):
    pass


class Employee:
    employee_count = 0
    # constructor
    # self is like this in hava
    # methods that start with __ means dunder methods or magic methods, basically encapsulation
    def __init__(self, weight, name, id) -> None:
        print("Employee created")
        self._weight = weight
        self._name = name
        self._id = id
        Employee.employee_count+=1
        print(f"No of employee(s): {Employee.employee_count}")
        if re.search(r"\d", self._id) is None:
            raise IDException("Id should contain numbers")

    def talk(self):
        print(f"I weight {self._weight:.5f}")

    def walk(self):
        print("employee walking")

    def __str__(self) -> str:
        return f"{self._weight} {self._name}";


class OfficeEmployee(Employee):
    def walk(self):
        print("Office worker is walking")


class TravelEmployee(Employee):
    def walk(self):
        print("Office worker is traveling")


class Class:
    def __init__(self, employee: OfficeEmployee) -> None:
        self._employee = employee

    def employee_talk(self):
        self._employee.talk()

    def employee_walk(self):
        self._employee.walk()


class Office(Employee):
    def __init__(self, weight, name, id, office_id):
        # Employee.__init__(weight,name,id) #this is superconstructor
        super().__init__(weight, name, id)
        self.__office_id = office_id

    def __str__(self):
        return f"Office employee from office: {self.__office_id}"


def main():
    emp1 = OfficeEmployee(5.122133, "Hontou", "A1")
    emp2 = TravelEmployee(5.555333, "Borutou", "A2")
    emp3 = TravelEmployee(5.555332, "Borutou", "A2")

    cl = Class(emp1)
    cl2 = Class(emp2)

    cl.employee_walk()
    cl2.employee_walk()


main()
