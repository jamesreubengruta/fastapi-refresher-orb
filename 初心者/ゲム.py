
import random
class ゲム:
    chosen_word = []
    max_guess = 11
    漢字 = ''
    def __init__(self,words) -> None:
        self.words = words

    def _init(self):
        self._number_guesses = 0
        self._letters = set()
        self._guesses = set()

    def _choose_word(self):
        rnd = (random.choice(list(self.words.items())))
        self.chosen_word =  rnd[1]
        self.漢字 =  rnd[0]

        for x in self.chosen_word:
            self._letters.add(x)


    def _display_word(self):
        current = []
        for g in self.chosen_word:
            if g in self._guesses:
                current.append(g)
            else:
                current.append('_')
        print(current)
    def _guess_letter(self):
        #ask a letter
        while self._number_guesses < self.max_guess and not (len(self._guesses) == len(self.chosen_word)):
            self._display_word()
            
            
            
            letter = input("choose a ひらがな:")
            if letter in self._letters and letter not in self._guesses:
                self._guesses.add(letter)
                diff = len(self._letters)-len(self._guesses)
                if diff == 0:
                    break
                else:
                    print(f'Awesome! only {diff} letter(s) left')
            else: 
                self._number_guesses+=1
                credits = self.max_guess-self._number_guesses
                print(f'Oops! you have {credits} chance(s) left')

    def _outcome(self):
        if self._number_guesses <  self.max_guess:
           self._win()
        else:
            self._game_over()

    def _game_over(self):
        print(f"Game over! The answer is {self.漢字} ({self.chosen_word}) \n\n")
    def _win(self):
         print(f"すごい! you got the answer! {self.漢字} ({self.chosen_word}) \n\n")
                            
    def run(self):
        while 1==1:
            print("___________________________")
            print("|  Welcome to Hiragana Man!    |")
            print("___________________________")
            input("Press <enter> for new game")
            self._init()
            self._choose_word()
            
            self._guess_letter()
            self._outcome()
            

    
    

import random

漢字 = {"母":"はは","六":"ろく","八":"はち","山":"やま","華":"はな","愛液":"あいえき","紫":"むらさき","緑":"みどり","赤":"あか","黒":"くろ","頭":"あたま","川":"かわ","冬":"ふゆ","夏":"なつ","酒":"さけ","歳":"さい","一万":"いちまん","幽霊":"ゆうれい","男":"おとこ","右":"みぎ","左":"ひだり","石":"いし","音":"おと","村":"むら","町":"まち","草":"くさ","好き":"すき","虫":"むし","門":"もん","空":"そら","足":"あし","下":"した","上":"うえ","手":"て","力":"ちから","車":"くるま","雨":"あめ","時":"じ","書":"しょうどしょうど"}


def main():
    x = ゲム(漢字)
    print(x.run())

main()