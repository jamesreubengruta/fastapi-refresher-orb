

人_dictionary = {
    '_id':'alph90',
    '名前':'alpha',
    '歳':33
}

print(人_dictionary)
print(人_dictionary.get('_id'))

人_dictionary['力'] = '催眠'
print(人_dictionary)

#for removing an item based on key
#人_dictionary.pop("歳")


for x,y in 人_dictionary.items():
    print(x,y)

print('------')

#for clearing dictionary
#人_dictionary.clear()
#for deleting
#del 人_dictionary

#copy by memory: if 人_dictionary_2 is updated, it also updates 人_dictionary
人_dictionary_2 = 人_dictionary

#copy by value, proper copying
人_dictionary_3 = 人_dictionary.copy