
sales_arg = {
    "月曜日":87,
    "火曜日":90,
    "水曜日":80,
    "木曜日":75,
    "金曜日":91,
    "土曜日":90,
    "日曜日":97,
}


def calculate_sales(sales_arg):
    sum_grades = 0
    for index,(x,y) in enumerate(sales_arg.items()):
            sum_grades+=y
    return round(sum_grades/index,3)

print(calculate_sales(sales_arg))

