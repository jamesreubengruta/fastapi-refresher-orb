class Machine():
    primary_key = 0

    def __init__(self, prefix, name):
        self._prefix = prefix
        self._name = name
        Machine.primary_key += 1
        self.id = f" Name: {self._name} ID: {self._prefix}{Machine.primary_key}"

    def __str__(self):
        return self.id

    @classmethod
    def get_primary(cls):
        return Machine.primary_key


class Mecha(Machine):
    def __str__(self):
        return f" Name: {self._name} Id: {self.id}"


def main():
    daimos = Mecha(prefix="DMS",name="Daimos")
    voltron = Machine(prefix="VLT",name="Voltron")
    print(daimos)
    print(voltron)
    print(Mecha.mro())

main()
