import re
from typing import List

from media_models import Media, Book, Movie, Podcast


def main():
    collection: List[Media] = [
        Podcast(_name="Joe Rogan", _description="Joe Rogan Conspiracies", _hosts="Rogan, Joe"),
        Book(_name="Harry Potter", _description="Wizardry Story", _author="J.K Rowling"),
        Book(_name="Kodansha Japanese Kanji Dictionary", _description="Japanese Dictionary", _author="ジェイムズ"),
        Book(_name="Holy Bible", _description="Holy Book", _author="God"),
        Book(_name="Chemical Engineering", _description="Chemical engineering maths and chemicals",
             _author="Rose Subdivision"),
        Book(_name="Software Engineering", _description="Computer", _author="Ada Lovace"),
        Book(_name="Philosophy of Stacks", _description="Philosophy", _author="James Gruta"),
        Book(_name="Little Prince", _description="French's Kid book", _author="Antoine De Saint Exupery"),
        Movie(_name="Lord of the Rings", _description="Epic Story", _director="Peter Jackson",
              _genre="Action Adventure"),
        Podcast(_name="Geek ko Alam", _description="Kids podcast about games", _hosts="Deco, Anthony"),
        Podcast(_name="Learning Japanese", _description="Podcast about learning Japanese",
                _hosts="Tria, Lianadel & Gruta, James Reuben"),

        Movie(_name="Alien Covenant",
              _description="Bound for a remote planet on the far side of the galaxy, members of the colony ship Covenant discover what they think to be an uncharted paradise",
              _director="Ridley Scott",
              _genre="Sci-fi, Horror"),
        Movie(_name="Titanic II", _description="B movie created by College Student", _director="Some college student",
              _genre="Drama, Suspense"),
    ]
    # print([print(f"{x}") for x in collection])
    gui(collection)


def search(word: str, collection: List[Media]):
    coll: List[Media] = []

    for md in collection:
        if re.search(rf"{word}", f"{md}", re.IGNORECASE):
            coll.append(md)

    return coll


def gui(collection: List[Media]):
    search_input = input("Enter search query for media:")
    results = search(search_input, collection)
    if len(results) > 0:
        [print(res) for res in results]
    else:
        print("Nothing happened!")
    main()


main()
