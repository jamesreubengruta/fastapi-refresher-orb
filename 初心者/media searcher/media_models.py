class Media:
    ___id = 0

    def __init__(self, _name, _description):
        Media.___id += 1
        self.id = Media.___id
        self.name = _name
        self.description = _description

    def __str__(self):
        return f" Media information: id: {self.id} name: {self.name} description: {self.description}"


# book, movie, podcast

class Book(Media):
    def __init__(self, _name, _description, _author):
        self.author = _author
        super().__init__(_name, _description)

    def __str__(self):
        return f" Book information: id: {self.id} name: {self.name} description: {self.description} author: {self.author}"


class Movie(Media):
    def __init__(self, _name, _description, _director, _genre):
        self.director = _director
        self.genre = _genre
        super().__init__(_name, _description)

    def __str__(self):
        return f" Movie information: id: {self.id} name: {self.name} description: {self.description} director: {self.director} genre: {self.genre}"


class Podcast(Media):
    def __init__(self, _name, _description, _hosts):
        self.hosts = _hosts
        super().__init__(_name, _description)

    def __str__(self):
        return f" Podcast information: id: {self.id} name: {self.name} description: {self.description} hosts: {self.hosts}"
