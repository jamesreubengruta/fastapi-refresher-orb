


class 漢字:
    #__ a.k.a dunder
    def __init__(self,name) -> None:
        self.name = name
    def __str__(self):
        return "漢字 string"
    def print(self):
        print(f"Kanji class {self.name} ")
    def _not(self):
        print("should be private method ")

火 = 漢字("火曜日")
火.print()
firestring = str(火)
print(firestring)
