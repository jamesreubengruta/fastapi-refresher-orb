漢字 = ["赤","紫","白","黒","青","緑"]
#returns index 2, 3rd item
print(漢字[2])
#returns last element of the index
print(漢字[-1])
#change 2nd index to pink
漢字[2] = "桃色"
print(漢字[2])
#prints the length
print('length of array',len(漢字))
#print sliced array
print(漢字[0:len(漢字)])
#add another value to list
漢字.append("暑い")
print('added 暑い : ',漢字[0:len(漢字)])
#removes 暑い from the list
漢字.remove("暑い")
print('removed 暑い : ',漢字[0:len(漢字)])
#removes index 2
漢字.pop(2)
print('removed index 2 with value of pink : ',漢字[0:len(漢字)])
#sorts the list
漢字.sort()
print('sorted list',漢字)
