# FastAPI Refresher Orb

- Refresher code for beginners and intermediate devs
- Basic repo explaining Python concepts
- FastAPI essentials
- RestAPI using Python

# Note: Some words are in japanese because im bored!

御免なさい！

## Basic commit rules

Use the following guidelines for commit

feat: A new feature for the user. (add login functionality)

fix: A bug fix for the user. (resolve issue with user authentication)

chore: Routine tasks, maintenance, or tooling changes. (update dependencies)

docs: Changes to documentation. (update installation instructions)

style: Code style changes (e.g., formatting). (format code according to style guide)

refactor: Code changes that neither fix a bug nor add a feature. (simplify user validation logic)

test: Adding or modifying tests. (add unit tests for some service)

## Installations

- pyenv
- "uvicorn[standard]"
- fastpi

## To run

python your_filename.py
