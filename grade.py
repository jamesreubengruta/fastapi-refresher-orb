グレイド = 77

#compact version
if グレイド >=90:
    print("S")
elif グレイド >=80:
    print("A")
elif グレイド >=70:
    print("B")
elif グレイド >=60:
    print("C")
else:
    print("F")

print(str(グレイド))


グレイド = 69
#another version showing explicit range
if グレイド >=90:
    print("S")
elif 80 <=グレイド <90:
    print("A")
elif 70 <=グレイド <80:
    print("B")
elif 60 <=グレイド<70:
    print("C")
else:
    print("F")

print(str(グレイド))